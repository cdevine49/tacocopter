class StoresController < ApplicationController
  def index
    params[:store] ||= {}
    taco_ids = params[:store][:taco_ids] || []
    salsa_ids = params[:store][:salsa_ids] || []
    @stores = Store.filter(taco_ids, salsa_ids)
    # @taco_stores = taco_ids.empty? ? Store.all :  Store.joins(:tacos)
    # .where(tacos: {id: taco_ids})
    # .group('stores.id')
    # .having('count(*) = ?', taco_ids.count)
    #
    # @salsa_stores = salsa_ids.empty? ? Store.all : Store.joins(:salsas)
    # .where(salsas: {id: salsa_ids})
    # .group('stores.id')
    # .having('count(*) = ?', salsa_ids.count)
    # @stores = @taco_stores & @salsa_stores
  end

  def search
    @tacos = Taco.all
    @salsas = Salsa.all
  end
end

private

def store_params
  params.require(:store).permit(:taco_ids, :salsa_ids)
end
