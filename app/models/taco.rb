class Taco < ActiveRecord::Base
  has_and_belongs_to_many :stores, uniq: true, join_table: :stores_tacos

end
