class Store < ActiveRecord::Base
  belongs_to :city
  has_many :car_washes
  has_and_belongs_to_many :tacos, uniq: true, join_table: :stores_tacos
  has_and_belongs_to_many :salsas, uniq: true, join_table: :stores_salsas

  def self.filter(taco_ids, salsa_ids)
    @taco_stores = taco_ids.empty? ? Store.all :  Store.joins(:tacos)
    .where(tacos: {id: taco_ids})
    .group('stores.id')
    .having('count(*) = ?', taco_ids.count)
    .order(:id)
    @salsa_stores = salsa_ids.empty? ? Store.all : Store.joins(:salsas)
    .where(salsas: {id: salsa_ids})
    .group('stores.id')
    .having('count(*) = ?', salsa_ids.count)
    .order(:id)
    @taco_stores & @salsa_stores
  end
end
