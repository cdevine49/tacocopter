FactoryGirl.define do
  factory :city do
    name "The City"
    allows_drones true

    factory :random_city do
      name Faker::Company.name
      allows_drones Faker::Boolean.boolean
    end
  end
end
