FactoryGirl.define do
  factory :store do
    name "The Store"
    city_id 1
    sells_beer true
    zagat_rating 5

    factory :random_store do
      name Faker::Company.name
      sells_beer Faker::Boolean.boolean
      zagat_rating Faker::Number.between(1, 10)
      city
    end
  end
end
