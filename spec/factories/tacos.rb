FactoryGirl.define do
  factory :taco do
    name "Mark Fitz"
    vegetarian false

    factory :random_taco do
      name { Faker::Name.name }
      vegetarian { Faker::Boolean.boolean }
    end
  end
end
