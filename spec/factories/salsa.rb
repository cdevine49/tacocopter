FactoryGirl.define do
  factory :salsa do
    name "Hot Tamale"

    factory :random_salsa do
      name { Faker::Lorem.words(2) }
    end
  end
end
