require 'rails_helper'

feature "the search form" do

  let!(:store1) { create(:random_store) }
  let!(:store2) { create(:random_store) }
  let!(:store3) { create(:random_store) }

  let!(:taco1) { create(:random_taco, stores: [store1, store2]) }
  let!(:taco2) { create(:random_taco, stores: [store2, store3]) }

  let!(:salsa1) { create(:random_salsa) }
  let!(:salsa2) { create(:random_salsa) }

  # let!(:salsa1) { Salsa.create(name: "test", stores: [Store.create]) }
  before(:each) do
    visit search_stores_path
  end

  scenario "tacos have the correct stores" do
    expect(taco1.stores).to include(store1, store2)
    expect(taco1.stores).to_not include(store3)

    expect(taco2.stores).to include(store2, store3)
    expect(taco2.stores).to_not include(store1)
  end

  scenario "salsas have the correct stores" do
    # salsa1.stores << [store1, store3]
    # expect(salsa1.stores).to include(store1, store3)
    # expect(salsa1.stores).to_not include(store2)

    # expect(salsa2.stores).to include(store1, store2)
    # expect(salsa2.stores).to_not include(store3)
  end

  scenario "has a search button" do
    expect(page).to have_button "Search"
  end

  scenario "has tacos listed" do
      expect(page).to have_content taco1.name
      expect(page).to have_content taco2.name
  end

  scenario "has salsas listed" do
    # expect(page).to have_content salsa1.name
    # expect(page).to have_content salsa2.name
  end

  scenario "select taco's stores are listed" do
    # check taco1.name
    # click_on 'submit'
    # expect(page).to include(store1, store2)
    # expect(page).to_not include(store3)
  end
end
