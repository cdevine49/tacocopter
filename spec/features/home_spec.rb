require 'rails_helper'

feature "the home page" do

  before(:each) do
    visit root_path
  end

  scenario "has a storefinder link" do
    expect(page).to have_content "StoreFinder"
  end

  scenario "clicking storefinder link" do
      click_on "StoreFinder"
      expect(page).to have_content "Search"
      expect(page).to have_content "Select Tacos to Find"
      expect(page).to have_content "Select Sauces to Find"
    end
end
